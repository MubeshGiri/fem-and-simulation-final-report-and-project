# Minimum CMake version required
cmake_minimum_required(VERSION 2.8)


# name
project(glew)


# settings & stuff
find_package(OpenGL REQUIRED)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fopenmp -m32")
include_directories( . )


# dynamic/static
option( BUILD_GLEW_DYNAMIC "Build glew static or dynamic" TRUE)
if(BUILD_GLEW_DYNAMIC )
  add_definitions( -DGLEW_BUILD )
else(BUILD_GLEW_DYNAMIC )
  add_definitions( -DGEW_STATIC )
endif(BUILD_GLEW_DYNAMIC )


# Files
set( HDRS GL/glew.h )
set( SRCS glew.c )

if(WIN32)
  set( HDRS ${HDRS} GL/wglew.h )
else(WIN32)
  set( HDRS ${HDRS} GL/glxew.h )
endif(WIN32)


# Build & Link
add_library( ${CMAKE_PROJECT_NAME} SHARED ${HDRS} ${SRCS} )
target_link_libraries( ${CMAKE_PROJECT_NAME} ${OPENGL_LIBRARIES} )
